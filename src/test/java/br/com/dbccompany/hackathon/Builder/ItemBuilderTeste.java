package br.com.dbccompany.hackathon.Builder;

import br.com.dbccompany.hackathon.Model.ItemModel;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

public class ItemBuilderTeste {

    String data = "[1-10-100,2-30-2.50,3-40-3.10]";

    @Test
    public void retornaListaDeItems() {
        ItemBuilder builder = new ItemBuilder();
        List<ItemModel> items = builder.run(data);
        assertEquals(3, items.size());
        assertEquals("2", items.get(1).getId());
        assertEquals(30, items.get(1).getQuantidade());
        assertEquals(2.5, items.get(1).getValor());
    }

    @Test
    public void divideEmBlocosDeItems() {
        ItemBuilder builder = new ItemBuilder();
        String[] blocos = builder.dividirEmBlocosDeItens(data);
        assertEquals("1-10-100", blocos[0]);
        assertEquals("2-30-2.50", blocos[1]);
        assertEquals("3-40-3.10", blocos[2]);
    }
}

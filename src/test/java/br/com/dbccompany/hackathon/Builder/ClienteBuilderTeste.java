package br.com.dbccompany.hackathon.Builder;

import br.com.dbccompany.hackathon.Model.ClienteModel;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class ClienteBuilderTeste {

    private String[] data = {"002", "2345675434544345", "Jose da Silva", "Rural"};

    @Test
    public void retornarEntidadeCliente() {
        ClienteBuilder builder = new ClienteBuilder();
        ClienteModel cliente = builder.run(data);
        assertEquals("2345675434544345", cliente.getCnpj());
        assertEquals("Jose da Silva", cliente.getNome());
        assertEquals("Rural", cliente.getAreaAtuacao());
    }
}

package br.com.dbccompany.hackathon.Security;

import br.com.dbccompany.hackathon.Entity.ConsultorEntity;
import br.com.dbccompany.hackathon.Repository.ConsultorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ImplementsUserDetailsService implements UserDetailsService {

    @Autowired
    private ConsultorRepository repository;


    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        Optional<ConsultorEntity> consultor = repository.findByLogin(login);

        if (consultor.isPresent()) {
            return consultor.get();
        }

        throw new UsernameNotFoundException("Consultor não encontrado");
    }
}

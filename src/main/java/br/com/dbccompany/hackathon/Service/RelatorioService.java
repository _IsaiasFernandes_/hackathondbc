package br.com.dbccompany.hackathon.Service;

import br.com.dbccompany.hackathon.Entity.RelatorioEntity;
import br.com.dbccompany.hackathon.HackathonApplication;
import br.com.dbccompany.hackathon.Repository.RelatorioRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class RelatorioService {

    private Logger logger = LoggerFactory.getLogger(HackathonApplication.class);

    @Autowired
    RelatorioRepository repository;

    @Transactional(rollbackFor = Exception.class)
    public RelatorioEntity salvar(RelatorioEntity entidade) {
        try {
            return repository.save(entidade);
        } catch( Exception e ) {
            logger.error("Erro ao salvar entidade: " + e.getMessage());
            throw new RuntimeException();
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public RelatorioEntity editar(RelatorioEntity entidade, Integer id) {
        try {
            entidade.setId(id);
            return repository.save(entidade);
        } catch ( Exception e ) {
            logger.error("Erro ao editar entidade: " + e.getMessage());
            throw new RuntimeException();
        }
    }

    public List<RelatorioEntity> todos() {
        try {
            return (List<RelatorioEntity>) repository.findAll();
        } catch ( Exception e ) {
            logger.error("Erro ao ver todo Relatorio " + e.getMessage());
            throw new RuntimeException();
        }
    }

    public Optional<RelatorioEntity> porId(Integer id) {
        try {
            return repository.findById(id);
        } catch ( Exception e ) {
            logger.error("Erro ao ver o Relatorio " + e.getMessage());
            throw new RuntimeException();
        }

    }

}

package br.com.dbccompany.hackathon.Builder;

import br.com.dbccompany.hackathon.Model.ItemModel;

import java.util.ArrayList;
import java.util.List;

public class ItemBuilder {
    public List<ItemModel> run(String s) {
        String[] blocosDeItens = dividirEmBlocosDeItens(s);
        List<ItemModel> items = new ArrayList<>();

        for (int i = 0; i < blocosDeItens.length; i++) {
            String[] itemDados = blocosDeItens[i].split("-");
            items.add(new ItemModel(
                    itemDados[0],
                    Integer.parseInt(itemDados[1]),
                    Double.parseDouble(itemDados[2])
            ));
        }
        return items;
    }

    public String[] dividirEmBlocosDeItens(String s) {
        return s.substring(1, s.length() - 1).split(",");
    }
}

package br.com.dbccompany.hackathon.Model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class VendaEValorTotalModel {

    private String id;
    private String nomeVendedor;
    private Double total;
}

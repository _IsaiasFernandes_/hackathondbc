package br.com.dbccompany.hackathon.Model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class VendedorModel {

    private String cpf;
    private String nome;
    private double salario;

}

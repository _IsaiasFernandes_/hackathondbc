package br.com.dbccompany.hackathon.Controller;

import br.com.dbccompany.hackathon.Entity.RelatorioEntity;
import br.com.dbccompany.hackathon.Service.RelatorioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/relatorio")
public class RelatorioController {

    @Autowired
    RelatorioService service;

    @GetMapping(value = "/todos")
    @ResponseBody
    public List<RelatorioEntity> todosRelatorio() {
        return service.todos();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public RelatorioEntity salvar(@RequestBody RelatorioEntity relatorio){
        return service.salvar(relatorio);
    }

    @GetMapping(value = "/ver/{id}")
    @ResponseBody
    public Optional<RelatorioEntity> RelatorioEspecifico(@PathVariable Integer id) {
        return service.porId(id);
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public RelatorioEntity editarRelatorio(@PathVariable Integer id, @RequestBody RelatorioEntity entity) {
        return service.editar(entity, id);
    }
}
